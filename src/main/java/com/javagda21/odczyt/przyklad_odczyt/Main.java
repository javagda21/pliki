package com.javagda21.odczyt.przyklad_odczyt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        // to co otworzyliśmy musimy zamknąć
        // pliki nie zapisują się od razu po dokonaniu akcji

        try {
            File plik = new File("/tmp/tekst_do_przeczytania"); // deskryptor pliku
            if (!plik.exists()) {
                plik.createNewFile();
            }

            BufferedReader bufferedReader = new BufferedReader(new FileReader(plik));

//            String linia;
//            do {
//                linia = bufferedReader.readLine();
//                 przeczytaj cały plik
//
//            } while (linia != null);

            String linia;
            while ((linia = bufferedReader.readLine()) != null) {

                System.out.println(linia);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
