package com.javagda21.odczyt.zad1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {


        try(BufferedReader bufferedReader = new BufferedReader(new FileReader("output_1.txt"))){
            String linia = bufferedReader.readLine();

            if(linia.equalsIgnoreCase("Hello World!")){
                System.out.println("W pliku znajduje sie odpowiedni napis.");
            }else{
                System.out.println("W pliku nie ma takiej linii.");
            }
        } catch (FileNotFoundException e) {
            System.out.println("Nie ma takiego pliku!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
