package com.javagda21.odczyt.zad1;

import java.util.Scanner;

public class MainPrzyklad {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        //############################# PRZYPADEK 1 #########################
        // ##################### ŹLE ############################
        do {
            System.out.println(scanner.next());
        } while (!scanner.next().equalsIgnoreCase("quit"));

        // wejście:
        // abecadło
        // Paweł
        // Gaweł
        // coś
        // nic
        // quit
        // quit

        // wyjście:
        // abecadło

        //############################# PRZYPADEK 2 #########################
        // ##################### Dobrze ############################
        String linia;
        do {
            linia = scanner.next();

            if (linia.equalsIgnoreCase("quit")) break;

            System.out.println(linia);
        } while (!linia.equalsIgnoreCase("quit"));
    }
}
