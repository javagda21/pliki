package com.javagda21.odczyt.zad2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("output_2.txt"))) {
            String linia = bufferedReader.readLine();

            System.out.println(linia.toUpperCase());
            System.out.println(linia.toLowerCase());
            System.out.println(linia.trim());

            String wynik = linia.trim();
            String wynikWynik = wynik.replaceFirst(wynik.charAt(0) + "", Character.toUpperCase(wynik.charAt(0)) + "");
            System.out.println(wynikWynik);
        } catch (FileNotFoundException e) {
            System.out.println("Nie ma takiego pliku!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
