package com.javagda21.odczyt.zad3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("output_3.txt"))) {
            int licznikSlow = 0, licznikLinii = 0;

            String linia;
            while ((linia = bufferedReader.readLine()) != null) {
                licznikLinii++;
//                licznikSlow += linia.trim().replace("  ", " ").replace(",", "").replace(".", "").split(" ").length;
                licznikSlow += linia.split("\\s+").length;
            }
            System.out.println("Licznik linii:" + licznikLinii);
            System.out.println("Licznik slow: " + licznikSlow);
        } catch (FileNotFoundException e) {
            System.out.println("Nie ma takiego pliku!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
