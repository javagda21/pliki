package com.javagda21.odczyt.zad4;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("formularz"))) {
            String linia;
            while ((linia = bufferedReader.readLine()) != null) {
                System.out.println(linia);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Nie ma takiego pliku!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
