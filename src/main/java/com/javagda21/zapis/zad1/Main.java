package com.javagda21.zapis.zad1;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    public static void main(String[] args) {

        try(PrintWriter writer = new PrintWriter(new FileWriter("output_1.txt"))){
            writer.println("Hello World!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
