package com.javagda21.zapis.zad2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try (PrintWriter writer = new PrintWriter(new FileWriter("output_2.txt"))) {

            writer.println(scanner.nextLine());

        } catch (IOException e) {
            // wypisuje każdą metodę aż do miejsca w którym wystąpił błąd. (stos wywołań)
            e.printStackTrace();

            // wypisuje tylko wiadomość o typie błędu
            System.out.println(e.getMessage());
        }
    }
}
