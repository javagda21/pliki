package com.javagda21.zapis.zad3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        File plik = new File("output_3.txt");
        plik.delete();

        String linia = "quit";
        do {
            try (PrintWriter writer = new PrintWriter(new FileWriter("output_3.txt", true))) {

                linia = scanner.nextLine();

                writer.println(linia);
                writer.flush(); // wymusza możliwie najwcześniejsze wypisanie treści do pliku

            } catch (IOException e) {
                // wypisuje każdą metodę aż do miejsca w którym wystąpił błąd. (stos wywołań)
                e.printStackTrace();

                // wypisuje tylko wiadomość o typie błędu
                System.out.println(e.getMessage());
            }

        } while (!linia.equalsIgnoreCase("quit"));
    }
}
