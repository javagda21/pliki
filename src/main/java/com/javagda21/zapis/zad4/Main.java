package com.javagda21.zapis.zad4;

import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj adres pliku:");
        String adres = scanner.nextLine();

        File file = new File(adres);
        if (file.exists()) {
            System.out.println("Istnieje");
            System.out.println(file.isDirectory() ? "Katalog" : "Plik");
            System.out.println("Rozmiar: " + file.length());
            System.out.println("Czas modyfikacji: " + file.lastModified());
            System.out.println("Zapis: " + file.canWrite());
            System.out.println("Odczyt: " + file.canRead());
        } else {
            System.out.println("Nie ma pliku");
        }
    }
}
