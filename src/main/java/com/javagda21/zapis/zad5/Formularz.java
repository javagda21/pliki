package com.javagda21.zapis.zad5;

public class Formularz {
    // Podaj swoje imie:
    private String imie;
    // Podaj swoje nazwisko:
    private String nazwisko;

    private int wiek;
    private int wzrost;
    private boolean czyMezczyzna;
    private int zarobki;

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public int getWzrost() {
        return wzrost;
    }

    public void setWzrost(int wzrost) {
        this.wzrost = wzrost;
    }

    public boolean isCzyMezczyzna() {
        return czyMezczyzna;
    }

    public void setCzyMezczyzna(boolean czyMezczyzna) {
        this.czyMezczyzna = czyMezczyzna;
    }

    public int getZarobki() {
        return zarobki;
    }

    public void setZarobki(int zarobki) {
        this.zarobki = zarobki;
    }

    @Override
    public String toString() {
        return "imie=" + imie + "\n" +
                "nazwisko=" + nazwisko + "\n" +
                "wiek=" + wiek + "\n" +
                "wzrost=" + wzrost + "\n" +
                "zarobki=" + zarobki + "\n" +
                "plec=" + (czyMezczyzna ? "facet" : "facetka") + "\n" +
                "##################################";

    }
}
