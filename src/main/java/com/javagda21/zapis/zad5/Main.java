package com.javagda21.zapis.zad5;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String odpowiedz;
        do {
            Formularz formularz = new Formularz();

            System.out.println("Podaj imie:");
            odpowiedz = scanner.nextLine();
            formularz.setImie(odpowiedz);

            System.out.println("Podaj nazwisko:");
            odpowiedz = scanner.nextLine();
            formularz.setNazwisko(odpowiedz);

            System.out.println("Podaj wiek:");
            odpowiedz = scanner.nextLine();
            // NumberFormatException - błąd parsoswania tekstu na liczbe
            int wiek = Integer.parseInt(odpowiedz); // zamiana tekstu (parsowanie) na liczbe
            formularz.setWiek(wiek);

            System.out.println("Podaj wzrost:");
            odpowiedz = scanner.nextLine();
            int wzrost = Integer.parseInt(odpowiedz);
            formularz.setWiek(wzrost);

            System.out.println("Jestes mezczyzna (t/n):");
            odpowiedz = scanner.nextLine();
            boolean czyMezczyzna = odpowiedz.equalsIgnoreCase("t");
            formularz.setCzyMezczyzna(czyMezczyzna);

            System.out.println("Podaj zarobki:");
            odpowiedz = scanner.nextLine();
            int zarobki = Integer.parseInt(odpowiedz);
            formularz.setZarobki(zarobki);

            try (PrintWriter writer = new PrintWriter(new FileWriter("formularz", true))) {
                writer.println(formularz);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Czy chcesz zamknąć program? Jeśli tak, wpisz 'quit':");
            odpowiedz = scanner.nextLine();
        } while (!odpowiedz.equalsIgnoreCase("quit"));
    }
}
