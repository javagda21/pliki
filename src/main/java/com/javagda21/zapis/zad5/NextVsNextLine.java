package com.javagda21.zapis.zad5;

import java.util.Scanner;

public class NextVsNextLine {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        System.out.println("Podaj linie:");
        String text = scanner.nextLine();
        System.out.println("Linia: " + text);

        System.out.println("Podaj slowo:");
        text = scanner.next();
        System.out.println("Slowo: " + text);

        System.out.println("Podaj slowo:");
        text = scanner.next();
        System.out.println("Slowo: " + text);

        System.out.println("Podaj slowo:");
        text = scanner.next();
        System.out.println("Slowo: " + text);

        System.out.println("Podaj linie:");
        text = scanner.nextLine();
        System.out.println("Linia: " + text);

        System.out.println("Podaj slowo:");
        text = scanner.next();
        System.out.println("Slowo: " + text);

        System.out.println("Podaj slowo:");
        text = scanner.next();
        System.out.println("Slowo: " + text);
    }
}
