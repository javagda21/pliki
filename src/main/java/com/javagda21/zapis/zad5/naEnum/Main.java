package com.javagda21.zapis.zad5.naEnum;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String odpowiedz;
        do {
            System.out.println("Podaj imie:");
            String imie = scanner.nextLine();

            System.out.println("Podaj nazwisko:");
            String nazwisko = scanner.nextLine();

            System.out.println("Podaj wiek:");
            odpowiedz = scanner.nextLine();
            // NumberFormatException - błąd parsoswania tekstu na liczbe
            int wiek = Integer.parseInt(odpowiedz); // zamiana tekstu (parsowanie) na liczbe


            System.out.println("Podaj wzrost:");
            odpowiedz = scanner.nextLine();
            int wzrost = Integer.parseInt(odpowiedz);

            Plec plec;
            do {
                try {
                    System.out.println("Podaj plec (kobieta/mezczyzna): ");
                    odpowiedz = scanner.nextLine();
                    plec = Plec.valueOf(odpowiedz.trim().toUpperCase());
                    break;
                } catch (IllegalArgumentException iae) {
                    continue;
                }
            } while (true);

            System.out.println("Podaj zarobki:");
            odpowiedz = scanner.nextLine();
            int zarobki = Integer.parseInt(odpowiedz);

            
            try (PrintWriter writer = new PrintWriter(new FileWriter("formularz", true))) {

                Formularz formularz;
                if(plec == Plec.KOBIETA){
                    System.out.println("Kolor włosów:");
                    String wlosy = scanner.nextLine();

                    System.out.println("Kolor oczu:");
                    String oczy= scanner.nextLine();

                    formularz = new FormularzKobiety(imie, nazwisko, wiek, wzrost, plec, zarobki, oczy, wlosy);
                    writer.println(formularz);
                }else{
                    System.out.println("Ile wyciskasz?");
                    int ileWyciska = Integer.parseInt(scanner.nextLine());

                    formularz = new FormularzMezczyzny(imie, nazwisko, wiek, wzrost, plec, zarobki, ileWyciska);
                    writer.println(formularz);
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Czy chcesz zamknąć program? Jeśli tak, wpisz 'quit':");
            odpowiedz = scanner.nextLine();
        } while (!odpowiedz.equalsIgnoreCase("quit"));
    }
}
